using UnityEngine;
using System.Collections;

[RequireComponent(typeof(GUITexture))]
public class TextureFullScreen : MonoBehaviour {
	private bool started = false;

	void Start () {
		StartCoroutine ("StartTimer");
	}
	void Update () {
		if (this.started) {
			GetComponent<GUITexture> ().pixelInset = new Rect (Screen.width / 2, Screen.height / 2, Screen.width / 32, Screen.height / 32);
			GetComponent<GUITexture> ().transform.localPosition = new Vector3 (0, 0, 999);
		}
	}

	private IEnumerator StartTimer(){
		yield return new WaitForSeconds(11f);
		this.started = true;
	}
}
