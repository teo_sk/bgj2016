﻿using UnityEngine;
using System.Collections;

public class level2_agro : MonoBehaviour {

	private bool already_agro = false;

	// Use this for initialization
	void Start () {
	
	}

	void OnTriggerEnter(Collider coll){
		if (already_agro == false) {
			already_agro = true;

			GameObject.FindObjectOfType<game_controller> ().War ();
		}
	}

	// Update is called once per frame
	void Update () {
	
	}
}
