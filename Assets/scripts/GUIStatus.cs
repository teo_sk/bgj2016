﻿using UnityEngine;
using System.Collections;

public class GUIStatus : MonoBehaviour {

	public float heartrate;
	public float fitness;

	private float scale = 1f;
	// Use this for initialization
	void Start () {
		scale = Screen.width / 1000f;
		GetComponent<RectTransform>().localScale = new Vector3 (scale,scale,1f);
	}

	void Change (){

		GetComponentInChildren < Animator> ().SetFloat("Blend",heartrate);
		imageControl[] ics = GetComponentsInChildren<imageControl> ();
		if (fitness > 9)
			fitness = 9;
		if (fitness < 0)
			fitness = 0;
		foreach(imageControl ii in ics){
			ii.num = (int)fitness;
		}
	}

	// Update is called once per frame
	void Update () {
		GameObject p = GameObject.Find ("player_cube");
		fitness = p.GetComponent<player_control> ().getFitness();

		Change ();
	}
}
