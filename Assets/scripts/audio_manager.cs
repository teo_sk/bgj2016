﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]

public class audio_manager : MonoBehaviour {

	public AudioClip[] fitMoves;
	public AudioClip[] fatMoves;
	public AudioClip[] fitJumps;
	public AudioClip[] fatJumps;

	public AudioClip breakThing;
	public AudioClip burp;
	public AudioClip clang;
	public AudioClip activate;

	public AudioClip heyyou;

	public AudioSource audio;

	private player_control player;

	private bool in_use_move = false;

	// Use this for initialization
	void Start () {
		GameObject playerObject = GameObject.Find("player_cube");
		this.player = playerObject.GetComponent<player_control> ();
	}

	public void PlayBreakSound() {
		audio.PlayOneShot (breakThing, 0.5f);
	}

	public void PlayBurp() {
		audio.PlayOneShot (burp, 0.5f);
	}

	public void PlayClang() {
		audio.PlayOneShot (clang, 0.5f);
	}

	public void PlayActivation() {
		audio.PlayOneShot (activate, 0.5f);
	}

	public void PlayHeyYou() {
		audio.PlayOneShot (heyyou, 0.5f);
	}

	// Update is called once per frame
	void Update () {
		int i = Random.Range (0, fitMoves.Length);

		if (Input.GetAxisRaw ("Horizontal") != 0 || Input.GetAxisRaw ("Vertical") != 0) {

			if (!in_use_move) {
				if (this.player.isFit()) {
					audio.PlayOneShot (fitMoves [i], 0.5f);					
				} else {
					audio.PlayOneShot (fatMoves [i], 0.5f);
				}

				in_use_move = true;
			}
		} else {
			in_use_move = false;
		}

		if (Input.GetKeyDown ("space")) {
			if (this.player.isFit()) {
				audio.PlayOneShot (fitJumps [i], 0.5f);					
			} else {
				audio.PlayOneShot (fatJumps [i], 0.5f);
			}
		}
	}
}
