﻿using UnityEngine;
using System.Collections;


public class rotate_door : activable {

	public float axisX = 0.0f;
	public float axisY = 1.0f;
	public float axisZ = 0.0f;
	public float rotateSpeed = 20.0f;
	public float rotateAcceleration = 4.0f;
	private float current_rot = 0.0f;

	private bool activated = false;
	override public void activate(){
		if (!activated) {
			activated = true;
			GameObject.Find ("audio_manager").GetComponent<audio_manager> ().PlayActivation ();
		}
	}

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		if (activated) {
			if (current_rot < rotateSpeed) {
				current_rot += rotateAcceleration * Time.deltaTime;
			}
			transform.Rotate (new Vector3 (axisX, axisY, axisZ), current_rot * Time.deltaTime);
		}
	}
}
