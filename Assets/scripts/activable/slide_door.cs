﻿using UnityEngine;
using System.Collections;


public class slide_door : activable {

	public float offsetX = 5.0f;
	public float offsetY = 0.0f;
	public float offsetZ = 0.0f;
	public float transitionTime = 1.0f;
	private Vector3 transitionVector;
	private Vector3 transitionDone;

	private bool activated = false;
	override public void activate(){
		if (!activated) {
			transitionVector = new Vector3 (offsetX,offsetY,offsetZ);
			transitionDone = new Vector3 (0.0f, 0.0f, 0.0f);
			activated = true;
			GameObject.Find ("audio_manager").GetComponent<audio_manager> ().PlayActivation ();
		}
	}

	// Use this for initialization
	void Start () {
	
	}

	// Update is called once per frame
	void Update () {
		if ( transitionDone.magnitude < transitionVector.magnitude ) {
			Vector3 delta = transitionVector / transitionTime * Time.deltaTime;
			transform.Translate ( transform.rotation * delta );
			transitionDone += delta;
		}
	}
}
