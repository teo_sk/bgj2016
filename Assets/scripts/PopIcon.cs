﻿using UnityEngine;
using System.Collections;

public class PopIcon : MonoBehaviour {

	public Texture tx;
	public float height;
	private Vector3 spos;
	public bool active = false;
	private float h = 0f;
	public float size = 50f;
	public float scale = 1f;
	// Use this for initialization
	void Start () {
		scale = Screen.width / 1000f;
	}

	// Update is called once per frame
	void OnGUI () {
		if (active)  {
			if (h < height)
				h += (height - h) / 10f;
			else
				h = height;
		} else {
			if (h > 0)
				h -= (h) / 10f;
			else
				h = 0;
		}

		spos = Camera.main.WorldToScreenPoint (transform.position + Vector3.up * h);
		GUI.DrawTexture (new Rect(spos.x-(scale*size)/2*h,Screen.height-spos.y-(size*scale)/2*h,(size*scale)*h,(scale*size)*h), tx);
	}

	void OnDestroy(){
	
	}
}
