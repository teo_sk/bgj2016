﻿using UnityEngine;
using System.Collections;

public class cube_trigger : MonoBehaviour {

	private AudioSource music;
	private AudioSource s1;
	private AudioSource s2;
	private AudioSource s3;

	// Use this for initialization
	void Start () {
		AudioSource[] sources = GetComponents<AudioSource> ();
		music = sources [0];
		s1 = sources [1];
		s2 = sources [2];
		s3 = sources [3];
		StartCoroutine(TriggerFlip());
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	IEnumerator TriggerFlip() {
		yield return new WaitForSeconds(3f);
		s1.Play ();
		yield return new WaitForSeconds(3f);
		s2.Play ();
		music.Play ();
		yield return new WaitForSeconds(1.5f);
		Destroy(GetComponent<Animator>());
		yield return new WaitForSeconds(2);
		s3.Play ();
		int i = 0;
		while(i < 5){
			transform.Rotate(Vector3.right,20);
			transform.Translate (Vector3.forward, Space.World);
			yield return new WaitForSeconds (0.002f);
			i++;
		}
	}
}
