﻿using UnityEngine;
using System.Collections;

public class ondestroy_activate : MonoBehaviour {

	public GameObject target;

	private activable targetScript;

	// Use this for initialization
	void Start () {
		if (target == null) {
			return;
		}
		targetScript = target.GetComponent<activable> ();
	}

	void OnDestroy() {
		if (targetScript == null) {
			return;
		}

		targetScript.activate ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
