﻿using UnityEngine;
using System.Collections;

public class dumbbell : powerup {

	public float tol = 6f;
	private PopIcon pi;
	private GameObject playObj;
	void Start(){
		playObj = GameObject.Find ("player_cube");
		pi = GetComponent<PopIcon>();
	}

	void OnTriggerEnter(Collider other) {
		playObj.GetComponent<player_control> ().setFitFat (player_control.FitFat.Fit);
		GameObject.Find ("audio_manager").GetComponent<audio_manager> ().PlayClang ();

		Destroy (gameObject);
	}

	void Update(){

		if((playObj.transform.position - this.transform.position).magnitude < tol)
			pi.active = true;
		else pi.active = false;
	}

}
