﻿using UnityEngine;
using System.Collections;

public class end_level : MonoBehaviour {

	public string sceneToLoad;

	// Use this for initialization
	void Start () {
	
	}

	void OnTriggerEnter() {
		if (sceneToLoad == null) {
			return;
		}
		Application.LoadLevel (sceneToLoad);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
