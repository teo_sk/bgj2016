﻿using UnityEngine;
using System.Collections;

public class ondestroy_greenlight : MonoBehaviour {

	public GameObject target;

	private Light targetLight;

	// Use this for initialization
	void Start () {
		if (target == null) {
			return;
		}
		targetLight = target.GetComponent<Light> ();
	}

	void OnDestroy() {
		if (targetLight == null) {
			return;
		}

		targetLight.intensity = 5.0f;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
