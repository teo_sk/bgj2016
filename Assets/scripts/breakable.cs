﻿using UnityEngine;
using System.Collections;

public class breakable : MonoBehaviour {

	public GameObject deathParticle;
	public Texture popIcon;
	private GameObject player;
	private GameObject go;
	private float break_by_mult = 1.4f;
	public float break_by_size = -1.0f;

	private Color originalColor;

	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player");
		go = this.gameObject;

		originalColor = new Color (0.0f,0.0f,0.0f);
		int num = 0;
		foreach(Renderer r in GetComponentsInChildren<Renderer>()){
			originalColor += r.material.color;
			num++;
		}
		originalColor /= num;
	}

	private bool canBeBrokenBy(Collider collider){

		float avg_size = GetComponent<Collider> ().transform.localScale.magnitude;
		float avg_size_other = collider.transform.localScale.magnitude;
		if (break_by_size > 0.0f) {
			if (break_by_size < avg_size_other) {
				return true;
			}
		} else {
			if (avg_size * break_by_mult < avg_size_other) {
				return true;
			}
		}
		return false;
	}

	void OnCollisionStay(Collision collision){
		if (collision.collider) {
			Rigidbody rb = collision.collider.GetComponent<Rigidbody> ();
			if (rb != null) {
				if ( canBeBrokenBy( collision.collider.GetComponent<Collider> () ) ) {
					Destroy ( go );
					GameObject.Find ("audio_manager").GetComponent<audio_manager> ().PlayBreakSound ();
					if (deathParticle) {
						GameObject exp = (GameObject)Instantiate (deathParticle);
						exp.transform.position = transform.position;
						float size = GetComponent<Collider> ().bounds.extents.magnitude;;
						exp.transform.localScale = new Vector3 ( size, size, size );
					}
				}
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (canBeBrokenBy (player.GetComponent<Collider> ())) {
			foreach (Renderer r in GetComponentsInChildren<Renderer>()) {
				PopIcon p = GetComponent<PopIcon> ();
				p.active = true;
				p.tx = popIcon;
				r.material.color = new Color (1.0f, 0.0f, 0.0f);
				r.material.SetColor ("Emission", new Color (1.0f, 0.0f, 0.0f));
				r.material.color = new Color (1.0f, 0.0f, 0.0f);
			}
		} else  {
			foreach(Renderer r in GetComponentsInChildren<Renderer>()){
				if (r.material.color.Equals (new Color (1.0f, 0.0f, 0.0f))) {
					PopIcon p = GetComponent<PopIcon> ();
					p.active = false;
					r.material.color = originalColor;
				}
		}
	}
}
}
