﻿using UnityEngine;
using System.Collections;

public class ondestroy_turn_hostile : MonoBehaviour {

	private game_controller target;

	// Use this for initialization
	void Start () {
		var gc = GameObject.FindGameObjectWithTag ("GameController");
		if (gc == null) {
			return;
		}
		target = gc.GetComponent<game_controller> (); 
	}

	void OnDestroy() {
		if (target == null) {
			return;
		}

		target.War ();
	}
}
