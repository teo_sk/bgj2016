﻿using UnityEngine;
using System.Collections;

public class postertextureset : MonoBehaviour {

	public Texture2D tx;
	public float sc;
	// Use this for initialization
	void Start () {
		if (sc > 0) {
			GetComponent<Renderer> ().material.SetTexture ("_MainTex", tx);
			this.transform.localScale = new Vector3 ((float)(sc*tx.width/1000), (float)(sc*tx.height/1000), 0.05f);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
