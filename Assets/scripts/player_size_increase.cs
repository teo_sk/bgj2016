﻿using UnityEngine;
using System.Collections;

public class player_size_increase : MonoBehaviour {

	player_control parent;

	private int num_contacts = 0;

	// Use this for initialization
	void Start () {
		parent = GetComponentInParent<player_control> ();
	}

	void OnTriggerEnter(){
		if( num_contacts == 0 ){
			parent.increaseSizeOnContact ();
		}
		num_contacts++;

		GetComponentInParent<roll_sound_generator>().PlaySound();
	}

	void OnTriggerExit(){
		num_contacts--;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
