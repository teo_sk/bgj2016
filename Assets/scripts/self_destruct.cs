﻿using UnityEngine;
using System.Collections;

public class self_destruct : MonoBehaviour {

	public float life = 5.0f;
	private float startTime;
	// Use this for initialization
	void Start () {
		startTime = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.time > startTime + life) {
			Destroy ( transform.gameObject );
		}
	}
}
