﻿using UnityEngine;
using System.Collections;

public class activator : MonoBehaviour {

	public activable targetObject;

	private GameObject player;
	private float activate_range_mult = 0.3f;
	public float activate_lower = -1.0f;
	public float activate_upper = -1.0f;
	private bool activated = false;
	public float tol = 6f;
	private Color originalColor;
	private PopIcon pi;
	// Use this for initialization
	void Start () {
		activated = false;
		player = GameObject.FindGameObjectWithTag ("Player");
		pi = GetComponent<PopIcon> ();
		originalColor = new Color (0.0f,0.0f,0.0f);
		int num = 0;
		foreach(Renderer r in GetComponentsInChildren<Renderer>()){
			originalColor += r.material.color;
			num++;
		}
		originalColor /= num;
	}

	private bool canBeActivatedBy(Collider collider){
		Bounds mb = GetComponent<Collider> ().bounds;
		Bounds b = collider.bounds;
		float avg_size = mb.extents.magnitude;
		float avg_size_other = b.extents.magnitude;
		if (activate_lower > 0.0f && activate_upper > 0.0f) {
			if (activate_lower < avg_size_other && activate_upper > avg_size_other) {
				return true;
			}
		} else {
			if ((avg_size-avg_size*activate_range_mult) < avg_size_other && (avg_size+avg_size*activate_range_mult) > avg_size_other) {
				return true;
			}
		}
		return false;
	}

	void OnCollisionStay(Collision collision){
		if (collision.collider) {
			Rigidbody rb = collision.collider.GetComponent<Rigidbody> ();
			if (rb != null) {
				if ( canBeActivatedBy( collision.collider.GetComponent<Collider> () ) ) {
					if (targetObject) {
						activated = true;
						targetObject.activate ();
					}
				}
			}
		}
	}

	// Update is called once per frame
	void Update () {
		if (!activated && canBeActivatedBy (player.GetComponent<Collider> ())) {
			foreach(Renderer r in GetComponentsInChildren<Renderer>()){
				if (pi != null){
					if ((player.transform.position - transform.position).magnitude < tol)
						pi.active = true;
					else pi.active = false;}
				r.material.color = new Color (0.0f, 1.0f, 0.0f);
			}
		} else if(GetComponent<Renderer> ().material.color.Equals(new Color (0.0f, 1.0f, 0.0f))) {
			foreach(Renderer r in GetComponentsInChildren<Renderer>()){
				if (pi != null)
					pi.active = false;
				r.material.color = originalColor;
			}
		}
	}
}