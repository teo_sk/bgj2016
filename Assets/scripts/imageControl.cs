﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class imageControl : MonoBehaviour {

	public int num;
	private Image imgcp;
	public Sprite[] sps;

	// Use this for initialization
	void Start () {
		imgcp = GetComponent<Image> ();
	}
	
	// Update is called once per frame
	void Update () {
		imgcp.sprite = sps[num];
	}
}
