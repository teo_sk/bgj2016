﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]

public class music_manager : MonoBehaviour
{

	public AudioClip peaceMusic;
	public AudioClip warMusic;
	public AudioSource audio;

	// Use this for initialization
	void Start ()
	{
		audio.clip = peaceMusic;
		audio.Play ();
	}
	
	// Update is called once per frame
	void Update ()
	{

	}

	public void PlayWarMusic ()
	{
		audio.Stop ();
		audio.clip = warMusic;
		audio.Play ();
	}

	public void PlayPeaceMusic ()
	{
		audio.Stop ();
		audio.clip = peaceMusic;
		audio.Play ();
	}
}

