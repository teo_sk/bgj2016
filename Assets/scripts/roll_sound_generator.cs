﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]

public class roll_sound_generator : MonoBehaviour
{
	public AudioClip[] list;
	public AudioSource audio;

	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{

	}

	public void PlaySound () {
		int number;
		number = Random.Range(0, list.Length);

		audio.PlayOneShot(list[number], 0.5f);
	}
}

