﻿using UnityEngine;
using System.Collections;

public class game_controller : MonoBehaviour {

	private music_manager audioManager;
	private audio_manager soundManager;
	private GUIStatus guiStatus;
	private bool isWar = false;

	public void War() {
		if (isWar) {
			return;
		}
		isWar = true;

		// notify enemies
		foreach (GameObject enemy in GameObject.FindGameObjectsWithTag("Enemy")) {
			var enemyController = enemy.GetComponent<enemy_script> ();
			enemyController.state = enemy_script.State.Hostile;
		}

		audioManager.PlayWarMusic ();
		soundManager.PlayHeyYou ();

		guiStatus.heartrate = 8;
	}

	public void Peace() {
		if (!isWar) {
			return;
		}
		isWar = false;


		foreach (GameObject enemy in GameObject.FindGameObjectsWithTag("Enemy")) {
			var enemyController = enemy.GetComponent<enemy_script> ();
			enemyController.state = enemy_script.State.Neutral;
		}

		audioManager.PlayPeaceMusic ();

		guiStatus.heartrate = 2;
	}

	// Use this for initialization
	void Start () {
		var audioObj = GameObject.Find ("audio_manager");
		audioManager = audioObj.GetComponent<music_manager> ();
		soundManager = audioObj.GetComponent<audio_manager> ();

		var guiStatsObj = GameObject.Find ("GUIstats");
		guiStatus = guiStatsObj.GetComponent<GUIStatus> ();

	}
	
	// Update is called once per frame
	void Update () {
		bool restartLevel = Input.GetKeyDown ("r");
		if (restartLevel) {
			Application.LoadLevel (Application.loadedLevel);
		}
	
	}
}
