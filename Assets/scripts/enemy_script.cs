﻿using UnityEngine;
using System.Collections;

public class enemy_script : MonoBehaviour {

	public enum State { Neutral, Hostile };

	public State state = State.Neutral;
	public Texture agroIcon;
	public Texture escIcon;
	private Rigidbody rb;
	public float maxSpeed = 5.0f;
	public float rollPower = 700.0f;
	private GameObject player;

	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player");
		rb = GetComponent<Rigidbody> ();
	}

	private void rotate(Vector3 direction){
		float power = rollPower * rb.mass;
		Vector3 torqueVector = new Vector3 (direction.z, 0.0f, -direction.x).normalized;
		Vector3 torque = Vector3.Scale( rb.inertiaTensor, Quaternion.Inverse( transform.rotation ) * (torqueVector*power) );
		rb.AddRelativeTorque (torque * Time.deltaTime);

		// rotating is just not enough, using some extra force will help stabilize it
		const float moveAssistForce = 4.5f;
		rb.AddForce (direction * rb.mass * moveAssistForce);
	}

	void OnCollisionEnter(Collision col){
		Collider c = col.collider;
		if(c.gameObject.GetComponent<player_control>()!=null)
			GameObject.Find("GameController").GetComponent<game_controller>().War();

	}

	// Update is called once per frame
	void Update () {
		if (state == State.Neutral) {
			GetComponent<PopIcon> ().active = false;
			return;
		} else {
			GetComponent<Light> ().intensity = 5.0f;
			GetComponent<PopIcon> ().active = true;
		}

		Bounds mb = GetComponent<Collider> ().bounds;
		Bounds b = player.GetComponent<Collider> ().bounds;
		float avg_size = mb.extents.magnitude;
		float avg_size_other = b.extents.magnitude;

		Vector3 direction = (player.transform.position - transform.position).normalized;

		if (avg_size < avg_size_other) {
			//ESCAPING
			direction *= -1.0f;
			GetComponent<PopIcon> ().tx = escIcon;
			//r.material.SetColor ("Emission", new Color (1.0f, 0.0f, 0.0f));
		} else {
			//FOLLOWING
			GetComponent<PopIcon> ().tx = agroIcon;
			//r.material.SetColor ("Emission", new Color (1.0f, 0.0f, 0.0f));
		}
		if (rb.velocity.magnitude < maxSpeed || Vector3.Dot( rb.velocity, direction) < 0.0) {
			rotate (direction);
		}
	}
}
