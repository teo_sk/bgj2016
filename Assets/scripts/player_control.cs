﻿using UnityEngine;
using System.Collections;

public class player_control : MonoBehaviour {

	public enum FitFat { Fit, Fat };

	private FitFat fitFat = FitFat.Fat;

	public float minTargetSize = 1.0f;
	public float maxTargetSize = 2.0f;

	private Rigidbody rb;
	private float rollPower = 800.0f;
	private float angularVelocityLimit = 4.0f;
	private float jumpForce = 500.0f;
	private float aligningSpeed = 80.0f;
	private float sizeChangeOnContact = 1.05f;
	private float growPerDist = 0.033f;

	private float playerSize = 1.0f;
	private float targetSize = 1.0f;
	const float sizeChangeSpeed = 0.4f;

	private bool contacts = false;
	private Vector3 lastPosition;

	public bool isFit() {
		return (this.fitFat == FitFat.Fit);
	}

	public void setFitFat(FitFat fitFat) {
		this.fitFat = fitFat;
	}

	public int getFitness() {
		float norm = maxTargetSize - minTargetSize;
		float actual = (targetSize - minTargetSize) / norm;
		int fitness = Mathf.RoundToInt (actual * 9);
		fitness = Mathf.Min (Mathf.Max (fitness, 0), 9);

		// Just in case
		return fitness;
	}

	public float getSize(){
		return playerSize;
	}

	public float getMass(){
		return playerSize * playerSize * playerSize; 
	}

	public void increaseSizeOnContact(){
//		if (fitFat == FitFat.Fit) {
//			targetSize = Mathf.Max(targetSize / sizeChangeOnContact, minTargetSize);
//		} else if (fitFat == FitFat.Fat) {
//			targetSize = Mathf.Min(targetSize * sizeChangeOnContact, maxTargetSize);
//		}
	}

	void OnCollisionStay(Collision collisionInfo) {
		contacts = true;
	}

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody> ();
		targetSize = playerSize;
		lastPosition = transform.position;
	}

	private void rotate(float horizontal, float vertical){
		float damper = angularVelocityLimit - Mathf.Clamp( rb.angularVelocity.magnitude, 0.0f, angularVelocityLimit );
		float power = rollPower/playerSize * damper;
		Vector3 torque = Vector3.Scale( rb.inertiaTensor, Quaternion.Inverse( transform.rotation ) * new Vector3 (vertical * power, 0.0f, horizontal * power) );
		rb.AddRelativeTorque (torque * Time.deltaTime);

		// rotating is just not enough, using some extra force will help stabilize it
		const float moveAssistForce = 8.5f;
		if (contacts) {
			rb.AddForce (new Vector3 (-horizontal * getMass () * moveAssistForce, 0.0f, vertical * getMass () * moveAssistForce));
		}
	}

	private void alignToAxes(){
		
		Vector3 transformed_x = transform.rotation * new Vector3(1.0f,0.0f,0.0f);
		Vector3 transformed_y = transform.rotation * new Vector3(0.0f,1.0f,0.0f);
		Vector3 transformed_z = transform.rotation * new Vector3(0.0f,0.0f,1.0f);
		Vector3 not_vertical = transformed_z;
		if (Mathf.Abs (transformed_x.y) < Mathf.Abs (transformed_y.y) && Mathf.Abs (transformed_x.y) < Mathf.Abs (transformed_z.y)) {
			not_vertical = transformed_x;
		} else if (Mathf.Abs (transformed_y.y) < Mathf.Abs (transformed_z.y)) {
			not_vertical = transformed_y;
		}

		if (rb.angularVelocity.magnitude < 0.1f && Mathf.Abs (not_vertical.y) < 0.1) {// only align if at rest (at least rotation)
			float x_dot = Vector3.Dot (not_vertical, new Vector3 (1.0f, 0.0f, 0.0f));
			float z_dot = Vector3.Dot (not_vertical, new Vector3 (0.0f, 0.0f, 1.0f));
			float dir = 1.0f;
			float damper = 1.0f;
			if (Mathf.Abs (x_dot) > Mathf.Abs (z_dot)) {
				dir = -Mathf.Sign( not_vertical.z ) * Mathf.Sign( not_vertical.x );
				damper = 1.0f - Mathf.Clamp (Mathf.Abs(x_dot), 0.1f, 0.7f);
			} else {
				dir = Mathf.Sign( not_vertical.x ) * Mathf.Sign( not_vertical.z );
				damper = 1.0f - Mathf.Clamp (Mathf.Abs(z_dot), 0.1f, 0.7f);
			}

			//transform.Rotate (new Vector3 (0.0f, damper * aligningSpeed * -dir * Time.deltaTime, 0.0f), Space.World);
		}
	}

	private void jump(){
		if (contacts) {
			rb.AddForce (new Vector3 (0.0f, jumpForce * getMass (), 0.0f));
		}
	}


	// Update is called once per frame
	void Update () {
		float hor_control = Input.GetAxis ("Horizontal");
		float ver_control = Input.GetAxis ("Vertical");
		bool doJump = Input.GetKeyDown ("space");

		rotate (-hor_control, ver_control);
		alignToAxes ();
		if (doJump) {
			jump ();
		}

		if (fitFat == FitFat.Fit && playerSize > targetSize) {
			playerSize -= sizeChangeSpeed * Time.deltaTime;
			transform.localScale = new Vector3 (playerSize, playerSize, playerSize);
			rb.mass = getMass();
			rb.ResetInertiaTensor ();
		} else if (fitFat == FitFat.Fat && playerSize < targetSize) {
			playerSize += sizeChangeSpeed * Time.deltaTime;
			transform.localScale = new Vector3 (playerSize, playerSize, playerSize);
			rb.mass = getMass();
			rb.ResetInertiaTensor ();
		}

		float flatDistance = (new Vector2(transform.position.x,transform.position.z) - new Vector2(lastPosition.x,lastPosition.z) ).magnitude;
		float grow = 1.0f;
		if (fitFat == FitFat.Fit) {
			grow -= flatDistance * growPerDist / playerSize;
		} else if (fitFat == FitFat.Fat) {
			grow += flatDistance * growPerDist / playerSize;
		}
		targetSize = Mathf.Clamp (targetSize * grow, minTargetSize, maxTargetSize);


		lastPosition = transform.position;
		contacts = false;
	}
}
